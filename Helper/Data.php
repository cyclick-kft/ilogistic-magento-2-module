<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Zenit\Ilogistic\Model\Enum\OrderStatus;
use Zenit\Ilogistic\Model\Enum\PaymentMethod;
use Zenit\Ilogistic\Model\Enum\ShippingMethod;

class Data extends AbstractHelper
{
    public const MAIN_KEY = 'ilogistic/';
    public const API_SETTINGS = 'api_settings/';
    public const SHIPPING_METHOD = 'shipping_method/';
    public const PAYMENT_METHOD = 'payment_method/';
    public const ORDER_STATUS = 'order_status/';

    public const GENERAL = 'general/';

    /**
     * @param ShippingMethod|PaymentMethod|OrderStatus $method
     * @param int|string|null $websiteId
     * @return array
     */
    private function getArrayConfigValue(ShippingMethod|PaymentMethod|OrderStatus $method, int|string|null $websiteId = null): array
    {
        $configType = match (true) {
            $method instanceof ShippingMethod => self::SHIPPING_METHOD,
            $method instanceof PaymentMethod => self::PAYMENT_METHOD,
            $method instanceof OrderStatus => self::ORDER_STATUS,
        };

        $config =
            $this->scopeConfig->getValue(
                self::MAIN_KEY . $configType . $method->value,
                ScopeInterface::SCOPE_WEBSITE,
                $websiteId
            )
            ?? [];

        if (is_string($config) && str_contains($config, ',')) {
            return explode(',', $config);
        }

        if (is_array($config) === false) {
            return [$config];
        }

        return $config;
    }

    public function isEnabled(int|string|null $websiteId = null): bool
    {
        return (bool)(
            $this->scopeConfig->getValue(
                self::MAIN_KEY . self::GENERAL . 'enabled',
                ScopeInterface::SCOPE_WEBSITE,
                $websiteId
            )
            ?? 0
        );
    }

    public function isDefaultEnabled(): bool
    {
        return (bool)$this->scopeConfig->getValue(self::MAIN_KEY . self::GENERAL . 'enabled');
    }

    public function isInDevelopmentMode(): bool
    {
        return (bool)$this->scopeConfig->getValue(self::MAIN_KEY . self::GENERAL . 'development');
    }

    /**
     * @param int|string|null $websiteId
     * @return string|null
     */
    public function getApiKey(int|string|null $websiteId = null): ?string
    {
        return $this->scopeConfig->getValue(
            self::MAIN_KEY . self::API_SETTINGS . 'api_key',
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    public function getDefaultApiKey(): ?string
    {
        return $this->scopeConfig->getValue(
            self::MAIN_KEY . self::API_SETTINGS . 'api_key',
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT
        );
    }

    /**
     * @param ShippingMethod $method
     * @param int|string|null $websiteId
     * @return array
     */
    public function getShippingMethod(ShippingMethod $method, int|string|null $websiteId = null): array
    {
        return $this->getArrayConfigValue($method, $websiteId);
    }

    /**
     * @param PaymentMethod $method
     * @param int|string|null $websiteId
     * @return array
     */
    public function getPaymentMethod(PaymentMethod $method, int|string|null $websiteId = null): array
    {
        return $this->getArrayConfigValue($method, $websiteId);
    }

    /**
     * @param int|string|null $websiteId
     * @return string
     */
    public function getWebshopAlias(int|string|null $websiteId = null): string
    {
        $aliasInput = $this->scopeConfig->getValue(
            self::MAIN_KEY . self::API_SETTINGS . 'alias',
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        ) ?? 'default';
        return "Magento-$aliasInput";
    }

    public function getDefaultAlias(): string
    {
        $aliasInput = $this->scopeConfig->getValue(self::MAIN_KEY . self::API_SETTINGS . 'alias') ?? 'default';
        return "Magento-$aliasInput";
    }

    /**
     * @param OrderStatus $orderStatus
     * @param int|string|null $websiteId
     * @param bool $shouldReturnMultiple
     * @return array|string|null
     */
    public function getOrderStatus(OrderStatus $orderStatus, int|string|null $websiteId = null, bool $shouldReturnMultiple = false): array|string|null
    {
        if ($shouldReturnMultiple) {
            return $this->getArrayConfigValue($orderStatus, $websiteId);
        }
        return $this->scopeConfig->getValue(
            self::MAIN_KEY . self::ORDER_STATUS . $orderStatus->value,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }
}
