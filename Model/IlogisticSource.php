<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\InventoryApi\Api\Data\SourceExtensionInterface;

class IlogisticSource extends AbstractModel implements SourceExtensionInterface
{
    protected bool $isPickupLocationActive = true;
    protected string $frontendName = 'iLogistic Source';
    protected string $frontendDescription = 'iLogistic warehouse management automatically synced stock source';

    /**
     * @inheritDoc
     */
    public function getIsPickupLocationActive(): ?bool
    {
        return $this->isPickupLocationActive;
    }

    /**
     * @inheritDoc
     */
    public function setIsPickupLocationActive($isPickupLocationActive): void
    {
        $this->isPickupLocationActive = (bool)$isPickupLocationActive;
    }

    /**
     * @inheritDoc
     */
    public function getFrontendName(): ?string
    {
        return $this->frontendName;
    }

    /**
     * @inheritDoc
     */
    public function setFrontendName($frontendName): void
    {
        $this->frontendName = (string)$frontendName;
    }

    /**
     * @inheritDoc
     */
    public function getFrontendDescription(): ?string
    {
        return $this->frontendDescription;
    }

    /**
     * @inheritDoc
     */
    public function setFrontendDescription($frontendDescription): void
    {
        $this->frontendDescription = (string)$frontendDescription;
    }
}
