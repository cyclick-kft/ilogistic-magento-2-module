<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Model\Config;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Shipping\Model\Config\Source\Allmethods;

class Shipping implements OptionSourceInterface
{
    /**
     * @param Allmethods $methods
     */
    public function __construct(
        protected readonly Allmethods $methods
    ) {
    }

    /**
     * @inheirtDoc
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => '', 'label' => __('-- Please Select --')],
            ...$this->methods->toOptionArray(),
        ];
    }
}
