<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Model\Config;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Payment\Model\Config\Source\Allmethods;

class Payment implements OptionSourceInterface
{
    /**
     * @param Allmethods $methods
     */
    public function __construct(
        protected readonly Allmethods $methods
    ) {
    }

    /**
     * @inheirtDoc
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => '', 'label' => __('-- Please Select --')],
            ...$this->methods->toOptionArray(),
        ];
    }
}
