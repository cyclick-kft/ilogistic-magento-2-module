<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Model\Enum;

enum PaymentMethod: string
{
    case Card = 'card';
    case Transfer = 'transfer';
    case COD = 'cod';

    /**
     * Returns iLogistic API compatible value
     *
     * @return string
     */
    public function getIlogisticMapping(): string
    {
        return match ($this) {
            self::Card => 'Bankkártyás fizetés',
            self::Transfer => 'Átutalásos fizetés',
            self::COD => 'Utánvétes fizetés',
        };
    }
}
