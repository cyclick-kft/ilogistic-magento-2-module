<?php

namespace Zenit\Ilogistic\Model\Enum;

enum OrderStatus: string
{
    case Approved = 'approved';
    case Preparing = 'preparing';
    case Packaged = 'packaged';
    case InTransit = 'delivery';
    case WaitingOnPackPoint = 'packpoint';
    case DamagedDuringDelivery = 'damaged';
    case Delivered = 'delivered';
    case Returned = 'returned';
    case Incorrect = 'incorrect';

    /**
     * @return string
     */
    public function getIlogisticMapping(): string
    {
        return match ($this) {
            self::Approved => 'Jóváhagyva',
            self::Incorrect => 'Hibás',
            self::Preparing => 'Összekészítés alatt',
            self::Packaged => 'Csomagolva',
            self::InTransit => 'Futárnak átadva',
            self::WaitingOnPackPoint => 'Csomagponton várakozik',
            self::DamagedDuringDelivery => 'Szállítás közben megsérült',
            self::Delivered => 'Kiszállítva',
            self::Returned => 'Visszáru',
        };
    }

    /**
     * @param string $ilogisticState
     * @return self
     */
    public static function getMagentoStatus(string $ilogisticState): self
    {
        return match ($ilogisticState) {
            'Jóváhagyva', 'Jóváhagyásra vár' => self::Approved,
            'Hibás' => self::Incorrect,
            'Összekészítés alatt' => self::Preparing,
            'Csomagolva' => self::Packaged,
            'Futárnak átadva' => self::InTransit,
            'Csomagponton várakozik' => self::WaitingOnPackPoint,
            'Szállítás közben megsérült' => self::DamagedDuringDelivery,
            'Kiszállítva' => self::Delivered,
            'Visszáru' => self::Returned,
        };
    }
}
