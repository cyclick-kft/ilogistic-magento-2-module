<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Model\Enum;

enum ShippingMethod: string
{
    case GLS = "gls";
    case Foxpost = "foxpost";
    case Personal = "personal";
    case Packpoint = "packpoint";
    case MPL = "mpl";
    case PacticPrimary = "pactic1";
    case PacticSecondary = "pactic2";
    case DHL = "dhl";
    case Pallet = "pallet";

    case ExpressOne = "express_one";
    case ExpressOnePallet = "express_one_pallet";

    case Other = "egyeb";

    /**
     * @return string
     */
    public function getIlogisticMapping(): string
    {
        return match ($this) {
            self::GLS => 'GLS',
            self::Foxpost => 'Foxpost',
            self::Personal => 'Személyes átvétel',
            self::MPL => 'MPL',
            self::DHL => 'DHL',
            self::Pallet => 'Raklapos szállítás',
            self::Packpoint => 'iLogistic csomagpont',
            self::PacticPrimary => 'Pactic Elsődleges',
            self::PacticSecondary => 'Pactic Másodlagos',
            self::ExpressOne => 'Express One',
            self::ExpressOnePallet => 'Express One Raklapos',
        };
    }

    public static function fromIlogisticMapping(string $ilogisticShippingMethod): ?self
    {
        return match ($ilogisticShippingMethod) {
            self::GLS->getIlogisticMapping() => self::GLS,
            self::Foxpost->getIlogisticMapping() => self::Foxpost,
            self::Personal->getIlogisticMapping() => self::Personal,
            self::MPL->getIlogisticMapping() => self::MPL,
            self::DHL->getIlogisticMapping() => self::DHL,
            self::Pallet->getIlogisticMapping() => self::Pallet,
            self::Packpoint->getIlogisticMapping() => self::Packpoint,
            self::PacticPrimary->getIlogisticMapping() => self::PacticPrimary,
            self::PacticSecondary->getIlogisticMapping() => self::PacticSecondary,
            self::ExpressOne->getIlogisticMapping() => self::ExpressOne,
            self::ExpressOnePallet->getIlogisticMapping() => self::ExpressOnePallet,
            default => null,
        };
    }
}
