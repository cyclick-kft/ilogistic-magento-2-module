<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Model;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonException;
use Zenit\Ilogistic\Exception\IlogisticApiException;
use Zenit\Ilogistic\Helper\Data;

class IlogisticApi
{
    private const ILOGISTIC_PRODUCTION_API = "https://api.ilogistic.eu";
    private const ILOGISTIC_STAGING_API = "https://devapi.ilogistic.eu";

    private readonly string $currentApiUri;

    /**
     * @param Client $httpClient
     * @param Data $config
     */
    public function __construct(
        private readonly Client $httpClient,
        Data                    $config,
    ) {
        $this->currentApiUri = $config->isInDevelopmentMode()
            ? self::ILOGISTIC_STAGING_API
            : self::ILOGISTIC_PRODUCTION_API;
    }

    /**
     * @param string $token
     * @param string $webshopAlias
     * @return string[]
     */
    private function prepareHeaders(string $token, string $webshopAlias): array
    {
        return [
            'User-Agent' => 'Magento-2',
            'Accept' => 'application/json',
            'Authorization' => "Bearer $token",
            'X-WebShop-Alias' => $webshopAlias,
        ];
    }

    /**
     * @param string $token
     * @return bool
     */
    public function validateToken(string $token): bool
    {
        try {
            $response = $this->httpClient->get(
                "{$this->currentApiUri}/validate",
                [
                    RequestOptions::HEADERS => [
                        'User-Agent' => 'Magento-2',
                        'Accept' => 'application/json',
                        'Authorization' => "Bearer $token"
                    ],
                ],
            );

            $decodedBody = json_decode(
                json: $response->getBody()->getContents(),
                associative: true,
                flags: JSON_THROW_ON_ERROR
            );

            return $response->getStatusCode() === 200 && $decodedBody['valid'];
        } catch (GuzzleException|JsonException $e) {
            return false;
        }
    }

    /**
     * @param string $token
     * @param string $webshopAlias
     * @return string
     * @throws IlogisticApiException
     */
    public function renewToken(string $token, string $webshopAlias): string
    {
        try {
            $response = $this->httpClient->get(
                "{$this->currentApiUri}/authentication/auth",
                [
                    RequestOptions::HEADERS => $this->prepareHeaders($token, $webshopAlias),
                ],
            );

            $bodyContent = $response->getBody()->getContents();
            $decodedBody = json_decode(
                json: $bodyContent,
                associative: true,
                flags: JSON_THROW_ON_ERROR
            );

            return $response->getStatusCode() === 200
                ? $decodedBody['token']
                : throw new IlogisticApiException(
                    $response->getStatusCode() . ' http status returned response: ' . $bodyContent,
                    2
                );
        } catch (GuzzleException|JsonException $e) {
            throw new IlogisticApiException("failed to process http request, error: " . $e->getMessage(), 2);
        }
    }

    /**
     * @param string $token
     * @param string $webshopAlias
     * @param array $skus
     * @return array
     * @throws IlogisticApiException
     */
    public function getStocks(string $token, string $webshopAlias, array $skus): array
    {
        try {
            $response = $this->httpClient->post(
                "{$this->currentApiUri}/products/stock",
                [
                    RequestOptions::HEADERS => $this->prepareHeaders($token, $webshopAlias),
                    RequestOptions::JSON => $skus,
                ],
            );

            $bodyContent = $response->getBody()->getContents();
            $decodedBody = json_decode(
                json: $bodyContent,
                associative: true,
                flags: JSON_THROW_ON_ERROR
            );

            return $response->getStatusCode() === 200
                ? $decodedBody
                : throw new IlogisticApiException(
                    $response->getStatusCode() . ' http status returned response: ' . $bodyContent,
                    2
                );
        } catch (GuzzleException|JsonException $e) {
            throw new IlogisticApiException("failed to process http request, error: " . $e->getMessage(), 2);
        }
    }

    /**
     * @param string $token
     * @param string $webshopAlias
     * @param array $orderJson
     * @return int
     * @throws IlogisticApiException
     */
    public function pushOrder(string $token, string $webshopAlias, array $orderJson): int
    {
        if ($orderJson['delivery']['company'] === null) {
            throw new IlogisticApiException("failed to process order, wrong shipping method mapping detected", 3);
        }

        if ($orderJson['payment']['type'] === null) {
            throw new IlogisticApiException("failed to process order, wrong payment method mapping detected", 3);
        }

        try {
            $response = $this->httpClient->post(
                "{$this->currentApiUri}/orders/orders",
                [
                    RequestOptions::HEADERS => $this->prepareHeaders($token, $webshopAlias),
                    RequestOptions::JSON => $orderJson,
                ],
            );

            $bodyContent = $response->getBody()->getContents();
            $decodedBody = json_decode(
                json: $bodyContent,
                associative: true,
                flags: JSON_THROW_ON_ERROR
            );

            return $response->getStatusCode() === 201
                ? $decodedBody['id']
                : throw new IlogisticApiException(
                    $response->getStatusCode() . ' http status returned response: ' . $bodyContent,
                    2
                );
        } catch (GuzzleException|JsonException $e) {
            throw new IlogisticApiException("failed to process http request, error: " . $e->getMessage(), 2);
        }
    }

    /**
     * @param string $token
     * @param string $webshopAlias
     * @param array $newProductList
     * @return mixed
     * @throws IlogisticApiException
     */
    public function pushProducts(string $token, string $webshopAlias, array $newProductList): mixed
    {
        try {
            $response = $this->httpClient->post(
                "{$this->currentApiUri}/products/products-multiple",
                [
                    RequestOptions::HEADERS => $this->prepareHeaders($token, $webshopAlias),
                    RequestOptions::JSON => $newProductList,
                ]
            );
            return json_decode(
                json: $response->getBody()->getContents(),
                associative: true,
                flags: JSON_THROW_ON_ERROR
            );
        } catch (GuzzleException|JsonException $e) {
            throw new IlogisticApiException('failed to process http request, error: ' . $e->getMessage(), 2);
        }
    }

    /**
     * @param string $token
     * @param string $webshopalias
     * @param int $orderId
     * @return string
     * @throws IlogisticApiException
     */
    public function getOrder(string $token, string $webshopalias, int $orderId): string
    {

        try {
            $response = $this->httpClient->get(
                "{$this->currentApiUri}/orders/order/{$orderId}",
                [
                    RequestOptions::HEADERS => $this->prepareHeaders($token, $webshopalias),
                ]
            );
            $bodyContent = $response->getBody()->getContents();
            $orderProperties = json_decode(
                json: $bodyContent,
                associative: true,
                flags: JSON_THROW_ON_ERROR
            );

            return isset($orderProperties['state']) && $response->getStatusCode() === 200
                ? $orderProperties['state']
                : throw new IlogisticApiException(
                    $response->getStatusCode() . ' http status returned response: ' . $bodyContent,
                    2
                );
        } catch (GuzzleException|JsonException $e) {
            throw new IlogisticApiException('failed to process http request, error: ' . $e->getMessage(), 2);
        }
    }
}
