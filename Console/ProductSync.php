<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Console;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Zenit\Ilogistic\Adapter\ProductAdapterInterface;
use Zenit\Ilogistic\Exception\IlogisticApiException;
use Zenit\Ilogistic\Helper\Data;
use Zenit\Ilogistic\Model\IlogisticApi;

class ProductSync extends Command
{
    protected const STORE = 'store';

    /**
     * @param IlogisticApi $api
     * @param Data $config
     * @param LoggerInterface $logger
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $productCollectionFactory
     * @param ProductAdapterInterface $productAdapter
     */
    public function __construct(
        protected readonly IlogisticApi            $api,
        protected readonly Data                    $config,
        protected readonly LoggerInterface         $logger,
        protected readonly StoreManagerInterface   $storeManager,
        protected readonly CollectionFactory       $productCollectionFactory,
        protected readonly ProductAdapterInterface $productAdapter,
    ) {
        parent::__construct('ilogistic:product-sync');
    }

    /**
     * @inheirtDoc
     */
    protected function configure(): void
    {
        $options = [
            new InputOption(
                self::STORE,
                's',
                InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL,
                'Optional storeId to push products from',
                [],
            )
        ];
        $this->setName('ilogistic:product-sync')
            ->setDescription('Push non existing products to iLogistic')
            ->setDefinition($options);

        parent::configure();
    }

    /**
     * @inheirtdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputWebsiteId = array_map(fn($option) => (int)$option, $input->getOption(self::STORE));

        $websites = empty($inputWebsiteId)
            ? array_map(fn($store) => (int)$store->getId(), $this->storeManager->getWebsites())
            : $inputWebsiteId;

        foreach ($websites as $website) {
            if ($this->config->isEnabled($website) === false) {
                continue;
            }

            $token = $this->config->getApiKey($website);
            $alias = $this->config->getWebshopAlias($website);

            $products = $this->productCollectionFactory
                ->create()
                ->addStoreFilter($website)
                ->setPageSize(50);

            $createdProducts = [];

            $lastPageNumber = $products->getLastPageNumber();
            $currentPage = 1;
            while ($lastPageNumber >= $currentPage) {
                $products = $this->productCollectionFactory
                    ->create()
                    ->addStoreFilter($website)
                    ->addAttributeToSelect('sku')
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('weight')
                    ->addAttributeToSelect('price')
                    ->setPageSize(50)
                    ->setCurPage($currentPage)
                    ->load();

                $newProducts = [];

                $skus = [];
                /** @var ProductInterface $product */
                foreach ($products as $product) {
                    $newProducts[] = $this->productAdapter->parseMagentoProduct($product);
                    $skus[] = $product->getSku();
                }
                $output->writeln('current page: ' . $products->getCurPage() . ' skus: ' . json_encode($skus));

                try {
                    $response = $this->api->pushProducts($token, $alias, $newProducts);
                    $createdProducts = array_merge($createdProducts, $response['id']);
                } catch (IlogisticApiException $e) {
                    $output->writeln('failed to send products');
                    $output->writeln($e->getMessage());
                }
                $currentPage++;
            }
            $output->writeln('created ' . count($createdProducts) . " products for website: $website");
            $output->writeln("from a total of $lastPageNumber pages");
        }

        return 0;
    }
}
