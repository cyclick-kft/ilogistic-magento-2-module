<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Setup;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\InventoryApi\Api\Data\SourceInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @param ResourceConnection $resource
     */
    public function __construct(
        protected readonly ResourceConnection $resource,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context): void
    {
        $sourceData = [
            SourceInterface::SOURCE_CODE => 'ilogistic_source',
            SourceInterface::NAME => 'iLogistic Source',
            SourceInterface::ENABLED => 1,
            SourceInterface::DESCRIPTION => 'iLogistic warehouse management automatically synced stock source',
            SourceInterface::LATITUDE => 47.474167,
            SourceInterface::LONGITUDE => 18.823611,
            SourceInterface::COUNTRY_ID => 'HU',
            SourceInterface::POSTCODE => '2051',
        ];

        $this->resource->getConnection()->insert(
            $this->resource->getTableName('inventory_source'),
            $sourceData
        );
    }
}
