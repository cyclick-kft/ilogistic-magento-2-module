<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;
use Zenit\Ilogistic\Adapter\OrderAdapterInterface;
use Zenit\Ilogistic\Exception\IlogisticApiException;
use Zenit\Ilogistic\Helper\Data;
use Zenit\Ilogistic\Model\Enum\OrderStatus;
use Zenit\Ilogistic\Model\IlogisticApi;

class OrderPlaceAfter implements ObserverInterface
{
    /**
     * @param IlogisticApi $api
     * @param LoggerInterface $logger
     * @param OrderAdapterInterface $adapter
     * @param OrderRepositoryInterface $orderRepository
     * @param Data $config
     */
    public function __construct(
        protected readonly IlogisticApi             $api,
        protected readonly LoggerInterface          $logger,
        protected readonly OrderAdapterInterface    $adapter,
        protected readonly OrderRepositoryInterface $orderRepository,
        protected readonly Data                     $config,
    ) {
    }

    /**
     * Push order to iLogistic and persist the ID generated in the iLogistic system
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        /** @var \Magento\Sales\Model\Order\Interceptor $order */
        $order = $observer->getEvent()->getData('order');

        $websiteId = $order->getStore()->getWebsiteId();
        if ($this->config->isEnabled($websiteId) === false) {
            return;
        }
        $token = $this->config->getApiKey($websiteId);
        $alias = $this->config->getWebshopAlias($websiteId);

        $startOrderStatuses = $this->config->getOrderStatus(OrderStatus::Approved, $websiteId, true);

        if ($order->isCanceled() || $order->isDeleted()) {
            return;
        }

        if (in_array($order->getStatus(), $startOrderStatuses, true) === false) {
            return;
        }

        $ilogisticOrder = $this->adapter->parseMagentoOrder($order, $websiteId);

        try {
            $orderId = $this->api->pushOrder($token, $alias, $ilogisticOrder);
            $order->setCustomAttribute('ilogistic_id', $orderId);
            $order->setData('ilogistic_id', $orderId);
            $this->orderRepository->save($order);

            $this->logger->info(
                "order created",
                [
                    'websiteId' => $websiteId,
                    'orderId' => $order->getIncrementId(),
                    'ilogisticOrderId' => $orderId
                ]
            );
        } catch (IlogisticApiException $e) {
            $this->logger->error(
                'failed to push order',
                [
                    'websiteId' => $websiteId,
                    'error' => $e->getMessage(),
                    'orderId' => $order->getIncrementId(),
                    'parsedModel' => json_encode($ilogisticOrder),
                ]
            );
        }
    }
}
