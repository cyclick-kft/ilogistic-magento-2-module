<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Cron;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Zenit\Ilogistic\Adapter\OrderAdapterInterface;
use Zenit\Ilogistic\Exception\IlogisticApiException;
use Zenit\Ilogistic\Helper\Data;
use Zenit\Ilogistic\Model\Enum\OrderStatus;
use Zenit\Ilogistic\Model\IlogisticApi;

class OrderCreateCron
{
    use PaginatedCronTrait;

    /**
     * @param IlogisticApi $api
     * @param Data $config
     * @param LoggerInterface $logger
     * @param StoreManagerInterface $storeManager
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteria
     * @param OrderAdapterInterface $adapter
     */
    public function __construct(
        protected readonly IlogisticApi             $api,
        protected readonly Data                     $config,
        protected readonly LoggerInterface          $logger,
        protected readonly StoreManagerInterface    $storeManager,
        protected readonly OrderRepositoryInterface $orderRepository,
        protected readonly SearchCriteriaBuilder    $searchCriteria,
        protected readonly OrderAdapterInterface    $adapter,
    )
    {
    }

    /**
     * @return $this
     */
    public function execute(): self
    {
        $websites = $this->storeManager->getWebsites();
        $stores = $this->storeManager->getStores(false);

        foreach ($websites as $website) {
            $websiteId = $website->getId();
            if ($this->config->isEnabled($websiteId) === false) {
                continue;
            }

            $token = $this->config->getApiKey($websiteId);
            $alias = $this->config->getWebshopAlias($websiteId);

            $approvedStatuses = $this->config->getOrderStatus(OrderStatus::Approved, $websiteId, true);
            $currentStores = array_map(
                fn($store) => $store->getId(),
                array_filter($stores, fn($store) => $store->getWebsiteId() === $websiteId)
            );
            $criteriaBuilder = $this->searchCriteria
                ->setPageSize(50)
                ->addFilter('ilogistic_id', null, 'null')
                ->addFilter('store_id', $currentStores, 'in')
                ->addFilter('status', $approvedStatuses, 'in');

            $searchCriteria = $criteriaBuilder->create();
            $orderCollection = $this->orderRepository->getList($searchCriteria);

            $lastPage = $this->getLastPageNumber($orderCollection->getTotalCount(), 50);
            $currentPage = 1;

            do {
                $orders = $orderCollection->getItems();
                foreach ($orders as $order) {
                    try {
                        $ilogisticOrder = $this->adapter->parseMagentoOrder($order, $websiteId);
                        $orderId = $this->api->pushOrder($token, $alias, $ilogisticOrder);
                        $order->setData('ilogistic_id', $orderId);
                        $this->orderRepository->save($order);
                        $this->logger->info(
                            "order created",
                            [
                                'websiteId' => $websiteId,
                                'orderId' => $order->getIncrementId(),
                                'ilogisticOrderId' => $orderId
                            ]
                        );
                    } catch (IlogisticApiException $e) {
                        $this->logger->error(
                            'failed to push order',
                            [
                                'websiteId' => $websiteId,
                                'error' => $e->getMessage(),
                                'orderId' => $order->getIncrementId()
                            ]
                        );
                    }
                }
                $currentPage = $searchCriteria->getCurrentPage() + 1;
                $searchCriteria = $searchCriteria->setCurrentPage($currentPage);
                $orderCollection = $this->orderRepository->getList($searchCriteria);
            } while ($lastPage >= $currentPage);
        }

        return $this;
    }
}
