<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Cron;

trait PaginatedCronTrait
{
    final protected function getLastPageNumber(int $totalOrders, int $pagination): int
    {
        return (int)ceil($totalOrders / $pagination);
    }
}
