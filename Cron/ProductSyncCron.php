<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Cron;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Zenit\Ilogistic\Console\ProductSync;

class ProductSyncCron
{
    public function __construct(
        protected readonly ProductSync $productSync,
    ) {
    }

    public function execute(): void
    {
        $input = new ArrayInput([]);
        $output = new NullOutput();
        $this->productSync->run($input, $output);
    }
}
