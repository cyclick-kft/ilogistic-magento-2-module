<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Cron;

use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Zenit\Ilogistic\Exception\IlogisticApiException;
use Zenit\Ilogistic\Helper\Data;
use Zenit\Ilogistic\Model\IlogisticApi;
use Magento\Framework\App\Config\Storage\WriterInterface;

class TokenRefreshCron
{
    use EventDispatchedCronTrait;

    /**
     * @param IlogisticApi $api
     * @param Data $config
     * @param LoggerInterface $logger
     * @param StoreManagerInterface $storeManager
     * @param WriterInterface $writer
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        protected readonly IlogisticApi          $api,
        protected readonly Data                  $config,
        protected readonly LoggerInterface       $logger,
        protected readonly StoreManagerInterface $storeManager,
        protected readonly WriterInterface       $writer,
        protected readonly ManagerInterface      $eventManager,
    ) {
        $this->eventName = 'token_update';
    }

    /**
     * Executes the cron event, updates iLogistic API key per store
     *
     * @return $this
     */
    public function execute(): static
    {
        $websites = $this->storeManager->getWebsites();

        $defaultApiKey = $this->config->getDefaultApiKey();
        $defaultAlias = $this->config->getDefaultAlias();

        if ($this->config->isDefaultEnabled()) {
            try {
                $newDefaultToken = $this->api->renewToken($defaultApiKey, $defaultAlias);
                $this->dispatchBefore(['token' => $newDefaultToken, 'website' => null]);
                $this->writer->save(Data::MAIN_KEY . Data::API_SETTINGS . 'api_key', $newDefaultToken);
                $this->dispatchAfter(['token' => $newDefaultToken, 'website' => null]);
                $this->logger->info('updated default api key');
            } catch (IlogisticApiException $e) {
                $this->logger->error(
                    "failed to update default api key",
                    ['error' => $e->getMessage()]
                );
            }
        }

        foreach ($websites as $website) {
            $websiteId = $website->getId();
            $this->logger->info('updating website', ['websiteId' => $websiteId]);
            if ($this->config->isEnabled($websiteId) === false) {
                continue;
            }
            $apiKey = $this->config->getApiKey($websiteId);
            $alias = $this->config->getWebshopAlias($websiteId);

            if ($apiKey === $defaultApiKey) {
                continue;
            }

            try {
                $newToken = $this->api->renewToken($apiKey, $alias);

                $this->dispatchBefore(['token' => $newToken, 'websiteId' => $websiteId]);
                $this->writer->save(
                    Data::MAIN_KEY . Data::API_SETTINGS . 'api_key',
                    $newToken,
                    ScopeInterface::SCOPE_WEBSITE,
                    $websiteId
                );
                $this->dispatchAfter(['token' => $newToken, 'websiteId' => $websiteId]);

                $this->logger->info("updated api key", ['websiteId' => $websiteId]);
            } catch (IlogisticApiException $e) {
                $this->logger->error(
                    "failed to update api key",
                    ['websiteId' => $websiteId, 'error' => $e->getMessage()]
                );
            }
        }

        return $this;
    }
}
