<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Cron;

use Magento\Framework\Event\ManagerInterface;

trait EventDispatchedCronTrait
{
    protected readonly ManagerInterface $eventManager;
    protected readonly string $eventName;

    /**
     * Dispatches an after event with the eventName and module identifier (ilogistic_{eventName}_after)
     *
     * @param array<string, mixed> $data passed down to the underlying event manager
     * @return void
     */
    final protected function dispatchAfter(array $data): void
    {
        $this->eventManager->dispatch("ilogistic_{$this->eventName}_after", $data);
    }

    /**
     * Dispatches a before event with the eventName and module identifier (ilogistic_{eventName}_before)
     *
     * @param array<string, mixed> $data passed down to the underlying event manager
     * @return void
     */
    final protected function dispatchBefore(array $data): void
    {
        $this->eventManager->dispatch("ilogistic_{$this->eventName}before", $data);
    }
}