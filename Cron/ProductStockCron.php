<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Cron;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Validation\ValidationException;
use Magento\Indexer\Model\IndexerFactory;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Psr\Log\LoggerInterface;
use Zenit\Ilogistic\Exception\IlogisticApiException;
use Zenit\Ilogistic\Helper\Data;
use Zenit\Ilogistic\Model\IlogisticApi;

class ProductStockCron
{
    public function __construct(
        protected readonly IlogisticApi               $api,
        protected readonly Data                       $config,
        protected readonly LoggerInterface            $logger,
        protected readonly StoreManagerInterface      $storeManager,
        protected readonly CollectionFactory          $productCollectionFactory,
        protected readonly SourceItemsSaveInterface   $itemsSave,
        protected readonly SourceItemInterfaceFactory $sourceItemInterfaceFactory,
        protected readonly IndexerFactory             $indexerFactory,
    ) {
    }

    public function execute(): static
    {
        $defaultStoreView = $this->storeManager->getDefaultStoreView();

        if ($this->config->isDefaultEnabled() === false) {
            $this->logger->warning(
                'product sync is unable to run because the module is disabled on the default config'
            );

            return $this;
        }

        $apiKey = $this->config->getDefaultApiKey();
        $alias = $this->config->getDefaultAlias();
        $source = 'ilogistic_source';

        if ($apiKey === null) {
            return $this;
        }

        $products = $this->productCollectionFactory
            ->create()
            ->addStoreFilter($defaultStoreView->getId())
            ->setPageSize(50);

        $lastPageNumber = $products->getLastPageNumber();
        $currentPage = 1;

        while ($lastPageNumber >= $currentPage) {
            $products = $this->productCollectionFactory
                ->create()
                ->addStoreFilter($defaultStoreView->getId())
                ->addAttributeToSelect('sku')
                ->setPageSize(50)
                ->setCurPage($currentPage);

            $skus = [];
            foreach ($products as $product) {
                $skus[] = $product->getSku();
            }

            try {
                $stocks = $this->api->getStocks($apiKey, $alias, $skus);
                $setStockParams = array_map(
                    function (string $sku) use ($stocks, $source) {
                        $sourceItem = $this->sourceItemInterfaceFactory->create();
                        $sourceItem->setQuantity((float)($stocks[$sku] ?? 0.00));
                        $sourceItem->setSku($sku);
                        $sourceItem->setStatus(
                            ($stocks[$sku] ?? 0.00) > 0
                                ? SourceItemInterface::STATUS_IN_STOCK
                                : SourceItemInterface::STATUS_OUT_OF_STOCK
                        );
                        $sourceItem->setSourceCode($source);
                        return $sourceItem;
                    },
                    $skus
                );
                $this->itemsSave->execute($setStockParams);

                $this->logger->info(
                    'updated stocks',
                    [
                        'skus' => $skus,
                    ]
                );
            } catch (CouldNotSaveException|InputException|ValidationException|IlogisticApiException $e) {
                $this->logger->error(
                    'failed to process product stock update',
                    [
                        'error' => $e->getMessage(),
                        'skus' => $skus,
                    ]
                );
            }
            $currentPage++;
        }

        try {
            $productPriceIndexer = $this->indexerFactory->create()->load('catalog_product_price');
            $categoryProductIndexer = $this->indexerFactory->create()->load('catalog_category_product');
            if ($productPriceIndexer->isValid()) {
                $productPriceIndexer->reindexAll();
            }
            if ($categoryProductIndexer->isValid()) {
                $categoryProductIndexer->reindexAll();
            }
        } catch (\Exception $e) {
            $this->logger->error(
                'failed to reindex products',
                [
                    'error' => $e->getMessage(),
                ]
            );
        }

        return $this;
    }
}
