<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Cron;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\ManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Zenit\Ilogistic\Exception\IlogisticApiException;
use Zenit\Ilogistic\Helper\Data;
use Zenit\Ilogistic\Model\Enum\OrderStatus;
use Zenit\Ilogistic\Model\IlogisticApi;

class OrderStatusCron
{
    use PaginatedCronTrait;
    use EventDispatchedCronTrait;

    /**
     * @param IlogisticApi $api
     * @param Data $config
     * @param LoggerInterface $logger
     * @param StoreManagerInterface $storeManager
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteria
     * @param ResourceConnection $connection
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        protected readonly IlogisticApi             $api,
        protected readonly Data                     $config,
        protected readonly LoggerInterface          $logger,
        protected readonly StoreManagerInterface    $storeManager,
        protected readonly OrderRepositoryInterface $orderRepository,
        protected readonly SearchCriteriaBuilder    $searchCriteria,
        protected readonly ResourceConnection       $connection,
        protected readonly ManagerInterface         $eventManager,
    ) {
        $this->eventName = 'order_status_update';
    }

    /**
     * Executes cron job and updates order statuses in all enabled stores
     *
     * @return $this
     */
    public function execute(): static
    {
        $websites = $this->storeManager->getWebsites();
        $stores = $this->storeManager->getStores(false);

        foreach ($websites as $website) {
            $websiteId = $website->getId();
            $token = $this->config->getApiKey($websiteId);
            $alias = $this->config->getWebshopAlias($websiteId);
            if ($this->config->isEnabled($websiteId) === false) {
                continue;
            }

            $orderStatuses = array_map(
                fn($status) => $this->config->getOrderStatus($status, $website->getId()),
                array_filter(
                    OrderStatus::cases(),
                    fn($status) => in_array($status, [OrderStatus::Returned, OrderStatus::Delivered, OrderStatus::Approved], true) === false
                )
            );
            $startStatuses = $this->config->getOrderStatus(OrderStatus::Approved, $websiteId, true);
            $currentStores = array_map(
                fn($store) => $store->getId(),
                array_filter($stores, fn($store) => $store->getWebsiteId() === $websiteId)
            );
            $criteriaBuilder = $this->searchCriteria
                ->setPageSize(50)
                ->addFilter('ilogistic_id', null, 'notnull')
                ->addFilter('store_id', $currentStores, 'in')
                ->addFilter('status', array_unique([...$startStatuses, ...$orderStatuses]), 'in');

            $searchCriteria = $criteriaBuilder->create();
            $orderCollection = $this->orderRepository->getList($searchCriteria);

            $lastPage = $this->getLastPageNumber($orderCollection->getTotalCount(), 50);
            $currentPage = 1;

            do {
                $orders = $orderCollection->getItems();

                foreach ($orders as $order) {
                    $ilogisticId = intval($order->getData('ilogistic_id'));

                    try {
                        $ilogisticOrderStatus = $this->api->getOrder($token, $alias, $ilogisticId);

                        $orderStatus = OrderStatus::getMagentoStatus($ilogisticOrderStatus);
                        $oldStatus = $order->getStatus();
                        [$newStatus] = $this->config->getOrderStatus($orderStatus, $websiteId, true);

                        if ($oldStatus !== $newStatus) {
                            $order->setStatus($newStatus);
                            $order->setState($this->getStatusToStateMapping($newStatus));
                            $order->addCommentToStatusHistory(
                                "iLogistic status update: $ilogisticOrderStatus",
                                $newStatus
                            );

                            $this->dispatchBefore(compact('order'));
                            $this->orderRepository->save($order);
                            $this->dispatchAfter(compact('order'));

                            $this->logger->info(
                                "order status updated",
                                [
                                    'websiteId' => $order->getStoreId(),
                                    'magentoOrderId' => $order->getIncrementId(),
                                    'ilogisticId' => $ilogisticId,
                                    'oldStatus' => $oldStatus,
                                    'newStatus' => $newStatus
                                ]
                            );
                        }
                    } catch (IlogisticApiException $e) {
                        $this->logger->error(
                            'failed to update order status',
                            [
                                'websiteId' => $order->getStoreId(),
                                'magentoOrderId' => $order->getIncrementId(),
                                'ilogisticId' => $ilogisticId,
                                'error' => $e->getMessage()
                            ]
                        );
                    }
                }
                $currentPage = $searchCriteria->getCurrentPage() + 1;
                $searchCriteria->setCurrentPage($currentPage);
                $orderCollection = $this->orderRepository->getList($searchCriteria);
            } while ($lastPage >= $currentPage);
        }

        return $this;
    }

    private function getStatusToStateMapping(string $status): string
    {
        $connection = $this->connection->getConnection();
        $stateMappingTableName = $connection->getTableName('sales_order_status_state');

        $query = $connection->select()
            ->from(['soss' => $stateMappingTableName], 'state')
            ->where('soss.status = ?', $status)
            ->limit(1);

        return $connection->fetchOne($query);
    }
}
