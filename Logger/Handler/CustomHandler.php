<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Logger\Handler;

use Magento\Framework\Logger\Handler\Base as BaseHandler;
use Monolog\Logger as MonologLogger;

class CustomHandler extends BaseHandler
{
    protected $loggerType = MonologLogger::DEBUG;
    protected $fileName = '/var/log/ilogistic.log';
}
