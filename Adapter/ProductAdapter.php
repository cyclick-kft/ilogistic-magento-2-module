<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Adapter;

use Magento\Catalog\Api\Data\ProductInterface as Product;

class ProductAdapter implements ProductAdapterInterface
{
    /**
     * @inheirtDoc
     */
    public function parseMagentoProduct(Product $product): array
    {
        return [
            'itemNumber' => $product->getSku(),
            'barCode' => [],
            'name' => $product->getName(),
            'type' => $product->getCategory()?->getName() ?? 'default',
            'category' => $product->isVirtual() ? 'virtual' : 'termek',
            'weight' => intval($product->getWeight() ?? 1),
            'width' => 1,
            'depth' => 1,
            'height' => 1,
            'fragile' => 1,
            'price' => intval($product->getPrice()),
            'hasSerial' => false,
            'hasExpirationDate' => false,
            'tax' => 27,
            'critical' => 0,
        ];
    }
}
