<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Adapter;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Zenit\Ilogistic\Helper\Data;
use Zenit\Ilogistic\Model\Enum\PaymentMethod;
use Zenit\Ilogistic\Model\Enum\ShippingMethod;

class OrderAdapter implements OrderAdapterInterface
{
    /**
     * @param Data $config
     */
    public function __construct(
        protected readonly Data           $config,
        protected CartRepositoryInterface $quoteRepository
    ) {
    }

    /**
     * @inheritDoc
     */
    public function parseMagentoOrder(OrderInterface $order, string|int|null $websiteId): array
    {
        $shippingAddress = $order->getShippingAddress();
        $billingAddress = $order->getBillingAddress();

        $shippingMethod = $this->parseShippingMethod($order->getShippingMethod() ?? 'other', $websiteId);
        $paymentMethod = $this->parsePaymentMethod(
            $order->getPayment()->getMethod() ?? 'invalid',
            $websiteId
        );

        return [
            'foreignId' => $order->getIncrementId(),
            'delivery' => [
                'name' => $shippingAddress->getName(),
                'email' => $shippingAddress->getEmail() ?? 'none@ilogistic.eu',
                'phoneNumber' => (string)$shippingAddress->getTelephone(),
                'country' => $shippingAddress->getCountryId(),
                'postCode' => $shippingAddress->getPostcode(),
                'city' => $shippingAddress->getCity(),
                'address' => implode(' ', $shippingAddress->getStreet()),
                'company' => $shippingMethod,
                'aptNumber' => $this->parseAptNumber($shippingMethod, $order),
            ],
            'billing' => [
                'name' => $billingAddress->getName(),
                'email' => $billingAddress->getEmail() ?? 'none@ilogistic.eu',
                'phoneNumber' => (string)$billingAddress->getTelephone(),
                'country' => $billingAddress->getCountryId(),
                'postCode' => $billingAddress->getPostcode(),
                'city' => $billingAddress->getCity(),
                'address' => implode(' ', $billingAddress->getStreet())
            ],
            'payment' => [
                'type' => $paymentMethod,
                'cost' => $order->getGrandTotal()
            ],
            'content' => $this->parseOrderItems($order),
        ];
    }

    /**
     * Maps all Magento 2 order items to an iLogistic API compatible array of items
     * @param OrderInterface $order
     * @return array<array{itemNumber: string, quantity: int}>
     */
    protected function parseOrderItems(OrderInterface $order): array
    {
        $orderItems = [];
        foreach ($order->getItems() as $item) {
            if (
                ($item->getParentItemId() !== null && $item->getProductType() === Configurable::TYPE_CODE)
                || $item->getProductType() !== Configurable::TYPE_CODE
            ) {
                $orderItems[] = $item;
            }
        }

        return array_map(
            fn(OrderItemInterface $item) => [
                'itemNumber' => $item->getSku(),
                'quantity' => intval($item->getQtyOrdered())
            ],
            $orderItems,
        );
    }

    /**
     * Parses shipping method based on the mapping provided by the module configuration
     *
     * @param string $shippingMethod Magento 2 shipping method name
     * @return string|null iLogistic shipping method name
     */
    protected function parseShippingMethod(string $shippingMethod, string|int|null $websiteId): ?string
    {
        foreach (ShippingMethod::cases() as $storedShippingMethod) {
            $storedMappings = $this->config->getShippingMethod($storedShippingMethod, $websiteId);
            if (in_array($shippingMethod, $storedMappings, true)) {
                return $storedShippingMethod->getIlogisticMapping();
            }
        }

        return null;
    }

    /**
     * Parses payment method based on the mapping provided by the module configuration
     *
     * @param string $paymentMethod Magento 2 payment method name
     * @return string|null iLogistic payment method name
     */
    protected function parsePaymentMethod(string $paymentMethod, string|int $websiteId): ?string
    {
        foreach (PaymentMethod::cases() as $storedPaymentMethod) {
            $storedMappings = $this->config->getPaymentMethod($storedPaymentMethod, $websiteId);
            if (in_array($paymentMethod, $storedMappings, true)) {
                return $storedPaymentMethod->getIlogisticMapping();
            }
        }

        return null;
    }

    /**
     * Maps the APT/Parcel locker/Pack point identifier to the corresponding field if necessary
     *
     * @param string $shippingMethod Mapped iLogistic shipping method name
     * @param OrderInterface $order
     * @return ?mixed
     */
    protected function parseAptNumber(string $shippingMethod, OrderInterface $order): mixed
    {
        $mappedShippingMethod = ShippingMethod::fromIlogisticMapping($shippingMethod);

        try {
            $quote = $this->quoteRepository->get($order->getQuoteId());
        } catch (NoSuchEntityException) {
            $quote = null;
        }

        return match ($mappedShippingMethod) {
            ShippingMethod::Foxpost => $quote?->getData('foxpost_parcelshop_id')
                ?? $order->getCustomAttribute('foxpost_shop_id')?->getValue(),
            ShippingMethod::MPL => $quote?->getData('mpl_parcel_id')
                ?? $order->getCustomAttribute('mpl_parcel_id')?->getValue(),
            ShippingMethod::GLS => $quote?->getData('gls_parcel_id')
                ?? $order->getCustomAttribute('gls_parcel_id')?->getValue(),
            default => null,
        };
    }
}
