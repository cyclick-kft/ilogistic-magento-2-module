<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Adapter;

use Magento\Sales\Api\Data\OrderInterface as Order;

interface OrderAdapterInterface
{
    /**
     * Convert a magento order to an iLogistic order API compatible associative array
     *
     * @param Order $order
     * @param string|int $websiteId
     * @return array{
     *     foreignId: string,
     *     delivery: array{
     *          name: string,
     *          email: string,
     *          phoneNumber: string,
     *          country: string,
     *          postCode: string,
     *          city: string,
     *          address: string,
     *          company: 'GLS'|'Foxpost'|'Személyes átvétel'|'MPL'|'DHL'|'Raklapos szállítás'|'iLogistic csomagpont'|'Pactic elsődleges'|'Pactic Másodlagos'|'Express One'|'Express One Raklapos',
     *          aptNumber?: string
     *     },
     *     billing: array{
     *           name: string,
     *           email: string,
     *           phoneNumber: string,
     *           country: string,
     *           postCode: string,
     *           city: string,
     *           address: string,
     *     },
     *     payment: array{type: 'Bankkártyás fizetés'|'Átutalásos fizetés'|'Utánvétes fizetés', cost: float},
     *     content: array<array{itemNumber: string, quantity: int}>
     *     }
     */
    public function parseMagentoOrder(Order $order, string|int|null $websiteId): array;
}
