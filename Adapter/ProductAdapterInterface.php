<?php

declare(strict_types=1);

namespace Zenit\Ilogistic\Adapter;

use Magento\Catalog\Api\Data\ProductInterface as Product;

interface ProductAdapterInterface
{
    /**
     * Convert a magento product to iLogistic product API compatible associative array
     *
     * @param Product $product
     * @return array{
     *     itemNumber: string,
     *     barCode: array<string>,
     *     name: string,
     *     type: string,
     *     category: 'virtual'|'termek',
     *     weight: int,
     *     width: int,
     *     depth: int,
     *     height: int,
     *     fragile: bool,
     *     price: int,
     *     hasSerial: bool,
     *     hasExpirationDate: bool,
     *     tax: int,
     *     critical: int
     * }
     */
    public function parseMagentoProduct(Product $product): array;
}
